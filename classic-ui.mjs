// Blank out the default images for all the compendium banners
Hooks.on('setup', () => {
    for (const t of ['Actor','Adventure','Cards','JournalEntry','Item','Macro','Playlist','RollTable','Scene']) { CONFIG[t].compendiumBanner = null }
})

Hooks.on('init', () => {
    // Reset the light window sizes to a less excessive size
    foundry.applications.sheets.AmbientLightConfig.DEFAULT_OPTIONS.position.width = 490

    // Turns out the sound config can't withstand any major tweaking without word-wrap making it painfully tall instead.  It really needs some tabs
    // Dropping the hint font size down to 12 does allow shrinking it to 540px instead of 560px though
    foundry.applications.sheets.AmbientSoundConfig.DEFAULT_OPTIONS.position.width = 540
})

// Drop the FilePicker size down from the 546px that is a bit excessive
Hooks.on('renderFilePicker', app => {app.position.width = 520; app.setPosition({width:520})})
