# Changelog

## 1.0.0 - V12 expansion

Expanded the scope to include other UI elements that could use streamlining.  Generally speaking, that includes most of the ApplicationV2 windows with large amounts of padding around elements.  

## 0.1.1 - Tiny manifest tweak

Fixed the manifest `verified` field

## 0.1.0 - Initial release

Initial release of the module

### Features

* Removes the default image for compendium backgrounds
* Removes the background around the compendium name (it's not needed for contrast without a background)
* Tightens the spacing of the rows (less space needed without artwork to show)
* Removes the banner image from the top of the popped out compendium

## 