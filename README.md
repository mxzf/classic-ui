# Classic UI

This module is designed to tweak the V11 compendium UI by removing the default background images and tightening the spacing to get a layout/interface closer to what was available in V10 and prior versions.

This module is designed to tweak the Foundry UI to streamline it and clean up certain areas with excessive whitespace/padding.  

In particular, it tweaks the V11 compendium UI to remove the default background images and tighten the spacing/layout and tweaks the V12 ApplicationV2 styling to achieve a tighter and less wasteful interface.  

This module primarily exists to make the changes I personally want to see in the UI, but I'm happy to entertain suggestions for other things to tweak or make optional via config settings.  Feel free to raise an issue on GitLab if there are things you would like to see changed.  

## Preview Images

### Compendium preview image
![example image](documentation/classic_compendium_ui.webp)

## ApplicationV2 preview image
![example image](documentation/classic_ui_ambient_light.webp)


## Installation

You can install the module manually using the manifest link
https://gitlab.com/mxzf/classic-ui/-/releases/permalink/latest/downloads/module.json